import os
import numpy as np
from PIL import Image
import pickle

# Path to the directory containing the images
directory = "D:/Data Pak Fitra/3000 gambar/"

# Lists to store the images and targets
inputFront = []
inputBack = []
target = []

# Iterate over files in the directory
for filename in os.listdir(directory):
    if filename.endswith(".jpg"):  # Adjust this as needed for your file types
        # Extract target from filename
        target_values = filename.split('-')[1]
        target.append([target_values])

        # Open image file
        img = Image.open(os.path.join(directory, filename))
        # Resize image to desired dimensions and convert to numpy array
        img_array = np.array(img.resize((384, 384)))
        # Normalize the image array
        img_array = img_array.astype('float32') / 255.0

        # Add the image array to the appropriate list based on its name
        if 'F' in filename:
            inputFront.append(img_array)
        elif 'B' in filename:
            inputBack.append(img_array)

# Convert the lists to numpy arrays
inputFront = np.array(inputFront)
inputBack = np.array(inputBack)
target = np.array(target)

# Check shapes
print(inputFront.shape)  # should be (1500, 384, 384, 3)
print(inputBack.shape)  # should be (1500, 384, 384, 3)
print(target.shape)  # should be (1500, 5)

# Store in a dictionary
data = {'inputFront': inputFront, 'inputBack': inputBack, 'target': target}

# Save the dictionary into a pickle file
with open('data.pickle', 'wb') as f:
    pickle.dump(data, f)
