import pickle

# Path to the pickle file
pickle_file = 'data.pickle'

# Load the data from the pickle file
with open(pickle_file, 'rb') as f:
    data = pickle.load(f)

# Extract the data
inputFront = data['inputFront']
inputBack = data['inputBack']
target = data['target']

# Print the shapes of the data
print('inputFront shape:', inputFront.shape)
print('inputBack shape:', inputBack.shape)
print('target shape:', target.shape)
